# DEPRECATED, please go to [new FAQ](https://bap.bitergia.com/faq/)

## What is the projects.json?

**Note: this information is deprecated, please [check our new FAQ](https://bap.bitergia.com/faq/)**


The projects.json describes the repositories (grouped by project and data sources) to be shown in the dashboards. It is
composed of three levels:
- First level: project names
- Second level: data sources and metadata
- Third level: data source URLs

## What is the setup.cfg?

**Note: this information is deprecated, please [check our new FAQ](https://bap.bitergia.com/faq/)**

The setup file holds the configuration to arrange all process underlying GrimoireLab. It is composed of different sections
that target general settings, phases, data storage info (`es_collection` and `es_enrichment`), backend and study sections. Details
of each section are available at: https://github.com/chaoss/grimoirelab-sirmordred

## How do the setup.cfg and projects.json relate each other?

**Note: this information is deprecated, please [check our new FAQ](https://bap.bitergia.com/faq/)**

The setup.cfg and projects.json are related via the backend section names (setup.cfg) and the data source names (projects.json). For instance,
giving the snippet of setup.cfg and projects.json, their relation is based on the name `github:repo`.

**setup.cfg**
```
[github:repo]
raw_index = github_grimoirelab_stats-raw
enriched_index = github_grimoirelab_stats
category = repository
no-archive = true
sleep-for-rate = true
```

**projects.json**
```
   "github:repo": [
        "https://github.com/chaoss/grimoirelab-toolkit"
    ]
```

## How to add a new repository?

**Note: this information is deprecated, please [check our new FAQ](https://bap.bitergia.com/faq/)**


A new repository is added by including it in the projects.json. The example below depicts two possible scenarios:
1. the repository is included in an existing project
2. the repository is part of a new project

```
{
    "grimoirelab": {
        "git": [
            "https://github.com/chaoss/grimoirelab-toolkit",
            "https://github.com/chaoss/grimoirelab-graal" <-- 1.
        ]
    },
    "secret-project": { <-- 2.
        "git": [
            "https://github.com/chaoss/grimoirelab-graal"
        ]
    },
}
```

## How to add a new MBox repository?

**Note: this information is deprecated, please [check our new FAQ](https://bap.bitergia.com/faq/)**

In order to start tracking a MBox repository, it is needed to: 
- subscribe our bot `barnowl@bitergia.com` to the target mailing list
- update the projects.json as follows:
    ```buildoutcfg
    ...
        "mbox": [
            "https://my-fantastic-group /home/bitergia/mboxes/barnowl_my-fantastic-group",
            "https://my-amazing-group /home/bitergia/mboxes/barnowl_my-amazing-group"
        ]
    ```
- open an issue in your GitLab support tracker

## How to enrich only a part of the raw data?

**Note: this information is deprecated, please [check our new FAQ](https://bap.bitergia.com/faq/)**


Let's suppose we want to collect all data available on `gerrit.chaoss.org`, which includes several projects. However,
we are interested in enriching only the projects labeled with `perceval` and `graal`. In this case, we have to
add `gerrit.chaoss.org` to the section `unknown`, and then include `gerrit.chaoss.org` with the proper filters (`--filter-raw=data.project`)
in the project we want. Note that the filter is defined based on the data obtained from the original repository, thus in
the example below, we assume that the original data includes an attribute with path `data.project`.

```
{
    "chaoss": {
        "gerrit": [
            "gerrit.chaoss.org --filter-raw=data.project:perceval",
            "gerrit.chaoss.org --filter-raw=data.project:graal",
        ]
    },
    "unknown": {
        "gerrit": [
            "gerrit.chaoss.org"
        ]
    }
}
```

## How to keep a repository in the projects.json when it doesn't exist anymore in upstream?

**Note: this information is deprecated, please [check our new FAQ](https://bap.bitergia.com/faq/)**


The repositories that don't exist anymore in upstream, but are present in the projects.json can filtered by 
appending `--filter-no-collection=true`. The collection process will ignore them, preventing in this way to log
useless errors.

```
{
    "chaoss": {
        "github": [
            "https:/github.com/chaoss/grimoirelab-perceval --filter-no-collection=true",
            "https:/github.com/chaoss/grimoirelab-sirmordred"
        ]
    }
}
```

## How to add labels to repositories?

**Note: this information is deprecated, please [check our new FAQ](https://bap.bitergia.com/faq/)**


The way it works is by adding a new parameter in projects.json to the repositories that should be labeled. That field is named `labels` and this is the expected format: `--labels=[examplelabel]` . Labels will be shown in the enriched index, while the attribute will be visible in the index pattern (it may be needed to refresh the index pattern).

```
"grimoire": {
    "git": [
        "https://github.com/chaoss/grimoirelab-perceval --labels=[collection]",
        "https://github.com/chaoss/grimoirelab-elk --labels=[enrichment]",
        "https://github.com/chaoss/grimoirelab-mordred --labels=[orchestrator, scheduler]",
        "https://github.com/chaoss/grimoirelab-sigils --labels=[visualization]"
    ]
}

```
Each repo can be "tagged" with several "labels" and later searched and filtered based on those labels.

The way to manage this is to create MRs against `projects.json` file to include labels. Labels will be stored in a field named `repository_labels` within the enriched indexes.

In order to get the labels updated for all items we need to enrich again the corresponding repositories/indexes, so adding or removing labels and having the changes reflected on the dashboard could take some time. Please try to group label changes as much as possible to avoid having too many re-enrichments.


## How to enable the tracking of a Slack instance?

**Note: this information is deprecated, please [check our new FAQ](https://bap.bitergia.com/faq/)**


- In your Slack instance, click the workspace name in the top left corner.
- Select `invite people` from the menu, then click `Members`.
- Enter the email address `owlbot@bitergia.com`.
- Under `Default Channels`, click `Edit/add` to choose the channels where `owlbot` will be added.
- Click on `Send invitations`.
- Once we receive the invitation, we will install the `OwlBot-app` in your workspace, and use its `OAuth Access Token`. The app will
  require the following scopes:
  - channels:history
  - channels:read
  - users:read
 
Depending on the workspace settings, the installation of the app may require the admin's approval.

## How to get the ID of Slack channel?

**Note: this information is deprecated, please [check our new FAQ](https://bap.bitergia.com/faq/)**


Access your Slack instance via browser and click on the specific channel.

The URL in your browser will change to something similar to `https://app.slack.com/client/TXXX/CYYY`.
`TXXX` is the team ID, while `CXXX` is the channel ID.

## How to enable the download of archives in Groups.io?

**Note: this information is deprecated, please [check our new FAQ](https://bap.bitergia.com/faq/)**


The permission to download archives can be enabled by checking the corresponding box within the `Admin>Settings>Message Policies` page. Note that if this box is checked, any subscriber can download the archives.

## Which are the data sources that not allow retrieving all their history?

**Note: this information is deprecated, please [check our new FAQ](https://bap.bitergia.com/faq/)**


### mbox
The past conversations are not retrieved, we can collect only the conversations going forward from the day when we started tracking the groups.

### slack
If you are using the free version, we can only retrieve the most recent 10,000 messages.
More info at https://slack.com/intl/en-es/help/articles/115002422943-Message-file-and-app-limits-on-the-free-version-of-Slack.

## How to track private repositories on GitHub/GitLab?

**Note: this information is deprecated, please [check our new FAQ](https://bap.bitergia.com/faq/)**


Issues, pull/merge request and commits can be tracked for private repositories on GitHub and GitLab. To do so, one of our mining accounts must be added to the organization containing the private repositories. Once added, we will have access to the
private repositories and we will generate an OAuth token to collect their data.

The data extracted from private repositories does not receive any special processing. Thus, the
information shown on the dashboard will be the same of the one of public repos (e.g., commit messages and hashes, file paths, issue comments). 
