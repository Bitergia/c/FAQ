# DEPRECATED, please go to [new FAQ](https://bap.bitergia.com/faq/)

## Contact Support

**Note: this information is deprecated, please [check our new FAQ](https://bap.bitergia.com/faq/)**


In case you find wrong data or you have any other question, please contact
us through the `support` 


### Using support portal

You'll find a direct link named `Contact` under the `About` entry on the top menu to [our support portal](https://support.bitergia.com):

![Contact Link](../images/contact.png)

### Using email

You can send us an email to support@bitergia.com and we will be glad to assist you. 

## Setup your Gitlab notification preferences

**Note: this information is deprecated, please [check our new FAQ](https://bap.bitergia.com/faq/)**


In order to setup your Gitlab notifications go to URL of the project you are interested in following (E.g. https://gitlab.com/Bitergia/c/GrimoireLab) and click on the bell icon. If you need more help about the notification options see the [following link]( https://gitlab.com/help/workflow/notifications.md#group-settings)

In case you are only interested in a specific repository under your project, you can also do this at repository level. (E.g. people only interested in tracking the support issues)

## Structure under Bitergia/c/

**Note: this information is deprecated, please [check our new FAQ](https://bap.bitergia.com/faq/)**

This documentation explains the structure used in Gitlab to provide the data
needed to set up a dashboard.

Inside the project you'll see different repositories created to allow you set
different access rules depending on the content. These are the different
repos and its goal:

* /support: this is the old place (before 1st of September 2022) where we were providing support. If you want to add your questions or bugs related to the service provided by Bitergia please contact support by email (support@bitergia.com) or by web on [support.bitergia.com](https://support.bitergia.com)
* /sources: you'll find here a JSON file with repositories monitored grouped by project
* /reports: PDF files with published quarterly reports
* /profiles: YAML file containing accounts and organization information for contributors. This is useful when you want to modify the information shown for specific contributors. Similar to what can be done with gitdm files but with more
data sources.
* /organizations: YAML file with organizations and domains mapping.

There is an example of setup at https://gitlab.com/Bitergia/c/CHAOSS, this configuration is used to provide the dashboard at https://chaoss.biterg.io

## Privacy settings

**Note: this information is deprecated, please [check our new FAQ](https://bap.bitergia.com/faq/)**


### Access to the dashboard

There are two main roles, the user with permission to see the dashboard and
the user with permission to edit it and accessing the indexes.

In case you want to change the visibility of the dashboard (or the indexes), contact the support team via ticket and let them know. See [How to contact support].


### Define who can access the data at Gitlab

Depending on your scope we recommend you to choose between three different configurations.

* _private_: everything private, only people with access to the repos will have access to the information. No tickets can be opened by external contributors.
* _normal_: the `profiles` repository remains private so in case you submit information about contributors it won't be visible. The rest of the trackers would be public so your community members will be able to provide feedback via issues, creating pull requests to add more organization-domain mappings to the dashboard or adding new repositories
* _public_: all information is publicly available. This is the ideal scenario to have more contributions from your community members. Same as 'normal' but community members will be also able to contribute to the profiles information, so
they will be able to improve/fix information about themselves or their team (everything via Pull Requets by default)

If you are community manager of a libre/open source community we strongly recommend **normal** or **public**

## How to Merge Request to a repository

**Note: this information is deprecated, please [check our new FAQ](https://bap.bitergia.com/faq/)**


There are few steps to follow to perform a new Merge Request in a Gitlab repository. In the following documentation, you will find the steps needed for modifying an affiliations file with the [GrimoireLab](https://github.com/bitergia/identities) format.

### Generate a new branch

In the main view of the repository, click on the '+' button and select 'Create Branch':

---

![alt text](../images/branch-generation-1.png)

---

There we will provide a name for the branch. In this case, as we are modifying identities, I will call the branch `update/user-affiliation`:

---

![alt text](../images/branch-generation-2.png)

---

### Modify the files

Once the branch is generated, make sure that the selected branch in the navbar is the new one, and select the file we want to edit. There, we will find a `edit` button, and we will click on it:

---

![alt text](../images/edit-file.png)

---

Clicking there will prompt us a text editor where we can modify the file:

---

![alt text](../images/unmodified-identities.png)

---

In the image above, three unmerged identities are displayed. What we will do here is, merge manually the important information of the three identities into one. After the proper modifications it will looks like:

---

![alt text](../images/modify-identity.png)

---

After that we will scroll down, make sure the Target Branch is the one we've generated, and click on the green `commit changes` button.

### Create the Merge Request

Once done, it will redirect us to a new page that will show a bar with a button to generate the desired Merge Request:

---

![alt text](../images/create-mr-after-commit.png)

---

In this page we will be able to modify the name of the Merge Request, and to select the source and target branch where to merge the changes. In this example, we are working on `update/user-affiliation` branch, and our target is `master`:

---

![alt text](../images/mr-submit1.png)

---

Scrolling down we will see all the modifications done in the identities file (commits). We can also click on the tab `Changes` to double check that the modifications done are correct. Finally, we can submit the Merge request:

---

![alt text](../images/mr-submit-2.png)

---

And that's it! This will generate the desired Merge request and will prompt you to a page like this:

---

![alt text](../images/final-mr.png)

---

The Bitergia team will be aware to double check the changes applied, and if everything is correct, it will be merged and applied to the dashboard.


## How does Bitergia handle feature requests? 

**Note: this information is deprecated, please [check our new FAQ](https://bap.bitergia.com/faq/)**


All customer's feature requests are evaluated and classified as: 
- improvements for the whole platform 
- ad-hoc developments (specific to single customers). 

In the case of improvements, they are included to the Bitergia's roadmap with a given priority, based on the priority of the other tasks part of the roadmap. The priority can be influenced by the customer by sponsoring the development. 

In the case of ad-hoc developments, the impact of the requested feature is assessed in terms of feasibility, maintenability and complexity. If the assessment is negative, the feature request is rejected and alternatives are proposed (when possible). For positive assessments, an estimate (hours and price) is communicated to the customer.

## Which are the checks performed at Bitergia?

**Note: this information is deprecated, please [check our new FAQ](https://bap.bitergia.com/faq/)**


The checks aim at granting the reliability and availability of the service as well as the quality and freshness of the data. They are listed below.
- [Zabbix](https://www.zabbix.com/), which is an open source network and application monitoring. We use it to make sure that your instance is up and running.
- [Logstash](https://www.elastic.co/logstash), which is an open source tool for managing logs. We use it to track the working of the instance and easily inspect the logs.
- [Kibiter](https://github.com/chaoss/grimoirelab-kibiter), which is a downstream of [Kibana](https://www.elastic.co/kibana), an open source tool for data visualization. It is the tool used to create dashboards to visualize your project data. The `Data Status` dashboard (included in your instance) provides information about the date when the data was lastly collected and enriched.
- [Jenkins](https://jenkins.io/), which is an open source automation server. We use it to execute jobs that monitor the trend and quality of the data, thus enabling the detection of data loss. 

In case of specific requests (e.g., meetings and events), we perform manual checks on the instance based on the customer needs.

[How to contact support]:README.md#contact-support

## How to add new members to your GitLab project?

**Note: this information is deprecated, please [check our new FAQ](https://bap.bitergia.com/faq/)**


You should create an issue that includes the GitLab usernames to be added. They will be granted with the `Developer` role [permission](https://gitlab.com/Bitergia/c/FAQ/-/blob/master/how-to-general/README.md#which-are-your-permissions-on-the-gitlab-repos) on all the project repositories.


## Which are your permissions on the GitLab repos?

**Note: this information is deprecated, please [check our new FAQ](https://bap.bitergia.com/faq/)**


Once you are added to the members of the GitLab project, you will be granted with `Developer` role permission on all the project repositories. This role allows to create and manage (e.g., set labels and assignee) merge requests as well as to create new branches and push to non-protected ones. You won't be able
to push to protected branches, add new team members or edit the project description. More details about the project member permissions is available at: https://gitlab.com/help/user/permissions#project-members-permissions.

## What are your permissions for the *.biterg.io dashboard?

**Note: this information is deprecated, please [check our new FAQ](https://bap.bitergia.com/faq/)**


Find below the permissions by default for your dashboard at `*.biterg.io`. Take into account the Anonymous user is not enabled
by default. In case you want to allow non logged users to access the dashboard [contact support](#contact-support) to change the setup.

| Permission/User | Anonymous *| Standard user
|---|---|---
|Access to dashboards (r/rw/- )| r | rw |
|Can create short links? | no| yes|

**Note:** the `Anonymous` user is the one used by non logged users and is not enabled unless you request for it

## What are your permissions for the *.biterg.io ES cluster (API)?

**Note: this information is deprecated, please [check our new FAQ](https://bap.bitergia.com/faq/)**


Find below the permissions by default for your Elasticsearch cluster at `*.biterg.io/data`. Take into account the 
Anonymous user is not enabled by default. In case you want to allow non logged users to access the 
dashboard [contact support](#contact-support) to change the setup.

| Permission/User | Anonymous *| Standard user
|---|---|---
|List indexes & aliases? |no | no|
|Access to indexes (r/rw/-) | r |r |
|Can modify aliases? | no | no|

**Note:** the `Anonymous` user is the one used by non logged users and is not enabled unless you request for it

## How to get new credentials?

**Note: this information is deprecated, please [check our new FAQ](https://bap.bitergia.com/faq/)**


In case you forgot your password to access the Gitlab tracker go to [the Gitlab Reset Password](https://gitlab.com/users/password/new) service

In order to get new credentials to access your dashboard and/or Elasticsearch cluster, please [contact support](#contact-support)

## What data sources does Bitergia Analytics support?

**Note: this information is deprecated, please [check our new FAQ](https://bap.bitergia.com/faq/)**


Bitergia supports more than 30 data sources: 

- Source Code Repositories:
	- Git
	- Bazaar
	- Mercurial
- Tickets & Issues:
	- GitHub
	- GitLab
	- JIRA
	- Bugzilla
	- Launchpad
	- Phabricator Maniphest
	- Redmine 
- Code Reviews:
	- Gerrit 
	- GitHub
	- GitLab 
- Continuous Integration:
	- Jenkins
- Mailing Lists:
 	- Askbot
	- Discourse
	- Gmane
	- Groups.io
	- Hyperkitty
	- Mbox files
	- NNTP
	- Pipermail
	- Stack Exchange
- Chat:
	- IRC
	- Slack
	- Telegram
- Wiki:
	- Confluence
	- MediaWiki
- Events:
	- Meetup
	- Mozilla Reps events 
- Others:
	- crates.io
	- Docker Hub
 	- RSS 

If you don't find the data source you are interested in, please open a GitLab support ticket and our engineering team will value it. On the other hand, you can also have a look at the [Data Sources Management.](https://gitlab.com/Bitergia/c/FAQ/-/tree/master/how-to-sources)
