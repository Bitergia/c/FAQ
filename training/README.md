# DEPRECATED, please go to [new FAQ](https://bap.bitergia.com/faq/)

# Training sessions for the Bitergia Analytics Platform

**Note: this information is deprecated, please [check our new FAQ](https://bap.bitergia.com/faq/)**



For getting as much benefit as possible from the Bitergia Analytics Platform, your subscription includes the following training sessions:

* Introduction to the Bitergia Analytics Platform
* Building visualizations and dashboards for the Bitergia Analytics Platform
* Querying and scripting the database supporting the Bitergia Analytics Platform

The third session will be useful to you if you or your team members are HAPPY working with Python scripts. Another option is to use that session to go deeper on dashboard use and customization.

Each session is delivered via videoconference using Google Hangouts, Zoom or some other tool that allows for voice talk and screen sharing. Each session lasts for about one hour and is usually presented on the Bitergia Analytics Platform deployed for the trainees. You can record the sessions in case you want to share them with your team members later.

If several sessions are to be scheduled, it is recommended to have a working week between them, so that people attending may have time to experience with the lessons learned and to get familiar with the aspects covered in each session.

Each session has a corresponding slide deck that can be used to follow the session and can be considered as roadmap for it.

## First Training Session: Introduction to the Bitergia Analytics Platform

**Note: this information is deprecated, please [check our new FAQ](https://bap.bitergia.com/faq/)**


This session aims to present the platform, including the dashboards and the tool used to deal with affiliations data. The topics covered are:

* Overview of the Bitergia Analytics Platform
* Using Kibana:
  + Mechanisms for filtering based on date or other data fields
  + Mechanisms for drilling down
  + Rearranging visualizations in a dashboard
  + Sharing and embedding views of a dashboard
  + Main functionalities of tables, pies, time series visualizations
  + Exporting data to spreadsheets
* Bitergia dashboards
  + How the different sections are structured
  + Panels for data sources
  + The Data Status panel
  + The About panel
* Organizations and affiliations
  + Using Hatstall to improve the data set

People attending the session are expected to have visited the dashboard previously, although no familiarity with it is needed. At the end of the seminar attendees may expect to know how to interpret visualizations in the different panels, how to drill down in the panels, and find specific information, and in general, how to use the dashboard for the most common tasks.

## Second Training Session: Building visualizations and panels for the Bitergia Analytics Platform

**Note: this information is deprecated, please [check our new FAQ](https://bap.bitergia.com/faq/)**


This session aims to present the editing capabilities of the dashboard, which are based on those of Kibana. Topics covered are:

* ElasticSearch and Kibana Basics
  + Fields, documents, indexes and index patterns
* Kibana editor
* Modifying and creating dashboards
  + Rearranging permanently visualizations in a dashboard
  + Changing names of visualizations in a dashboard
  + Including new visualizations in a dashboard
  + Creating and saving dashboards
* Visualizations and searches
  + Creating metrics, tables, pie charts, time series visualizations and others
  + General recommendations on creating visualizations
* Creating a custom dashboard

People attending this session are expected to have covered the topics of the session "Introduction to Bitergia Analytics Platform", either by having attending the session or by having previous experience with a real dashboard. After the session, attendees may expect to learn about how to modify and create panels and visualizations in the dashboard.

## Third Training Session: Querying and scripting the database supporting the Bitergia Analytics Platform

**Note: this information is deprecated, please [check our new FAQ](https://bap.bitergia.com/faq/)**


This session aims to present some advanced aspects related to the exploitation of the data used to back the dashboard. This data is stored in an ElasticSearch instance, and exploitation will be done by querying and scripting. Topics covered are:

* Basic ElasticSearch querying
* How the dashboard arranges the data in ElasticSearch
* Data available for the different data sources
* Some examples of specific queries to answer some simple data needs.
* Basics of Python scripting to access the data in ElasticSearch
* Examples of some Python scripts to get some specific data from the database.

People attending this session are expected to be familiar with the dashboard, and with the contents of the two sessions Introduction to Bitergia Analytics Platform" and "Building visualizations and panels for the Bitergia Analytics Platform". They are expected to be familiar with tools to query HTTP APIs, such as `curl`, with the Python programming language, and with basic concepts of noSQL databases. After the session, they may expect to know the basics about how to query the data to answer simple questions, and to produce Python scripts using data in the ElasticSearch database.
