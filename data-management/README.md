# DEPRECATED, please go to [new FAQ](https://bap.bitergia.com/faq/)

## Offboarding

**Note: this information is deprecated, please [check our new FAQ](https://bap.bitergia.com/faq/)** 

We thank you for the time you have been with us. When your contract expires we will decommission and remove your support tracker and your Bitergia Analytics instance. The data sets will be removed too in order to fulfill the **GDPR** law, in case you want to get a copy of your data sets, before leaving please let us know, so we can prepare everything.

The procedure we follow to provide you the data sets is explained in the [Export User Data procedure](data-management/README.md#export-user-data-procedure). 

## Export User Data procedure

**Note: this information is deprecated, please [check our new FAQ](https://bap.bitergia.com/faq/)** 

If you need your data, you can request it at any time.

**Procedure:**

1.Open a **GitLab support issue** asking for the data.

2.From the person who is going to get access to the data we need:

- The **Keybase username** to send the data safely.    
- a **GPG key** attached to his/her Keybase user. We'll use it to encrypt the data files.
- a **SSH key** to give access to download the files. We can use GitLab/Github public key if you have it added.

3.Bitergia generates the dump.

4.When the data is ready you'll be provided with a username and an IP address.

5.Use the **username to connect** to that server using SSH/SCP. You 'll need to use the private SSH key provided to us:

    ssh username@address -i /path/to/private_key

6.The encrypted files will be at the user's home directory:

    -rw-r--r-- 1 username username 1.4G Mar  2 12:02 file_raw.tar.7z.gpg
    -rw-r--r-- 1 username username 6.0M Mar  2 13:36 file_identities.tar.7z.gpg
    -rw-r--r-- 1 username username 142M Mar  2 13:08 file_enriched.tar.7z.gpg

7.After downloading the files, you should be able to decrypt them using your PGP key.From the host where you looged in to Keybase, run:

    keybase pgp decrypt -i file_raw.tar.7z.gpg -o file_raw.tar.7z

8.Now you are able to extract the files from the 7z archive with any compression utility that supports 7-zip format.

9.Inform Bitergia that you have been able to extract the files in order we can dispose or delete the data.

**Some help:**

1.How to add the **GPG key** to your user in Keybase:

    keybase pgp gen    # if you need a PGP key
    keybase pgp select # if you already have one in GPG
    keybase pgp import # to pull from stdin or a file

2.How to add your **SSH key to your Github** account:
	you can read it on this [link](https://docs.github.com/en/github/authenticating-to-github/adding-a-new-ssh-key-to-your-github-account).

3.How to add your **SSH key to your GitLab** account:
	you have all the information [here](https://docs.gitlab.com/ee/ssh/#add-an-ssh-key-to-your-gitlab-account).
